import { Appi3Page } from './app.po';

describe('appi3 App', function() {
  let page: Appi3Page;

  beforeEach(() => {
    page = new Appi3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
