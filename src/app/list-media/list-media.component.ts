import { MediaService } from './../services/media.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-media',
  templateUrl: './list-media.component.html',
  styleUrls: ['./list-media.component.css']
})
export class ListMediaComponent implements OnInit {

  constructor(private mediaService: MediaService) { }

  data:any = [];
  ngOnInit() {
       this.mediaService.getAllMedia().subscribe(
        (json) => {
          this.data = json;
        }
      );
  }

}
