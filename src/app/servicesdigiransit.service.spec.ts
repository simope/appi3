/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ServicesdigiransitService } from './servicesdigiransit.service';

describe('ServicesdigiransitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServicesdigiransitService]
    });
  });

  it('should ...', inject([ServicesdigiransitService], (service: ServicesdigiransitService) => {
    expect(service).toBeTruthy();
  }));
});
