import { Http } from '@angular/http';
import { Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class MediaService {

//http:Http;
  constructor(private h:Http) { //this.http = h;
    }

  getAllMedia() {
    return this.h.get('http://media.mw.metropolia.fi/wbma/media').map((res: Response) => res.json());
  }

}
