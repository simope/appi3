import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';

@Injectable()
export class DigitransitService {


  constructor(private h:Http) { }

  fetchInfo(stopName: string) {
    let headers = new Headers({ 'Content-Type': 'application/graphql' });
    let options = new RequestOptions({ headers: headers });

    const url: string =  "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql";
    let data = `
      {
        stops(name: "${stopName}") {
          patterns {
            name
          }
        }
      }
    `;

    return this.h.post(url, data, options).map((res:Response) => res.json());
  }

}
