import { DigitransitService } from './../services/digitransit.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
  styleUrls: ['./routes.component.css']
})
export class RoutesComponent implements OnInit {

  constructor(private digitransit: DigitransitService) { }

  private data:any = [];
  private name:string = "kuusitie";
  ngOnInit() {
       this.digitransit.fetchInfo(name).subscribe(
        (json) => {
          console.log(json);
          this.data = json.data.stops;

        }
      );
  }

}
